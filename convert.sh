#!/bin/bash

SRC=$1
DST="${1%.txt}.md"

pandoc -f dokuwiki -t gfm -o "$DST" "$SRC"

# We had a tendency to use image links in Dokuwiki where we really meant an internal link. (ie, '{{ foo }}' vs '[foo]')
#
# This is most prominent where we link to a pdf.
#
# So we look for image links (of the form '![foo](bar)' and strip off the leading '!', converting them to internal links.
#
# It's probably fractionally easier doing this after we've converted to Markdown given it's a fairly simple regex.
sed -i -r 's/!(\[.*\]\(.*\.pdf\))/\1/' "${DST}"

# Convert all links targets to lowercase. There's a mismatching between link names in the Dokuwiki source, and the case of the backing files.
sed -i -r 's/(\[.*?\]\()(.*)(\))/\1\L\2\3/' "${DST}"

# Remove ampersands from internal wiki links. They _appear_ to have been stripped somewhere before Dokuwiki stored the files.
sed -i -r 's/(\[.*\]\(.*)\&(.*\))/\1\2/' "${DST}"

# Convert double spaces in link targets to single spaces. This collapses double spaces that arise when we remove an ampersand from "foo & bar" (as it becomes "foo  bar").
#
# It's a bit of a hack, but it seems to work for our data.
sed -i -r 's/(\[.*\]\(.*)%20(%20.*\))/\1\2/' "${DST}"

# Convert spaces in link targets to underscores. This conforms with the backing files from Dokuwiki
sed -i -r -e :a -e 's/(\[.*\]\(.*)%20(.*\))/\1_\2/; ta' "${DST}"
